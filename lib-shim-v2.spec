#needsrootforbuild
%global __cargo_skip_build 0
%global _debugsource_packages 1
%global _debuginfo_subpackages 1
%define _unpackaged_files_terminate_build 0
%define debug_package %{nil}

Name:           lib-shim-v2
Version:        0.0.1
Release:        11
URL:            https://gitee.com/openeuler/lib-shim-v2
Source:         %{name}-%{version}.tar.gz
Source1:	libc.tar.gz
Summary:        lib-shim-v2 is shim v2 ttrpc client which is called by iSulad.
Group:          Application/System
License:        Mulan PSL v2

ExclusiveArch:  x86_64 aarch64 loongarch64 riscv64

Patch01:        0001-lib-shim-v2-add-support-for-loongarch64.patch 
Patch02:        0002-simplify-Store-api-remove-unnecessary-validation.patch
Patch03:        0003-Check-if-a-path-is-abstract-before-connection.patch
Patch04:        0004-Add-vsock-support.patch
Patch05:        0005-sandbox-sandbox-api-update.patch
Patch06:        0006-add-adapter-for-runc-sandboxer.patch
Patch07:        0007-update-dependent-protobuf-and-ttrpc-version.patch
Patch08:        0008-support-stats-cmd.patch
Patch09:        0009-add-riscv-support.patch

BuildRequires:  rust
BuildRequires:  cargo
BuildRequires:  rust-packaging
BuildRequires:  gcc
%ifarch riscv64
BuildRequires:  protobuf-compiler
%endif

%description
Based on Rust programming language, as a shim v2 ttrpc client, it is called by iSulad.

%package devel
Summary: shim v2 ttrpc client
Group:   Libraries
ExclusiveArch:  x86_64 aarch64 loongarch64 riscv64
Requires: %{name} = %{version}-%{release}

%description devel
the %{name}-libs package contains Libraries for shim v2 ttrpc client 

%prep
%autosetup -p1
%cargo_prep
%cargo_generate_buildrequires

%build
sed -i '/\[source.crates-io\]/{n;d}' ./.cargo/config
sed -i '/\[source.local-registry\]/{n;d}' ./.cargo/config
sed -i '/\[source.local-registry\]/a directory = "vendor"' ./.cargo/config
%ifarch loongarch64
rm -rf vendor/libc
tar -xf %{SOURCE1} -C vendor 
%endif
%cargo_build -a

%install
mkdir -p ${RPM_BUILD_ROOT}/{%{_libdir},%{_includedir}}
install -m 0644 shim_v2.h ${RPM_BUILD_ROOT}/%{_includedir}/shim_v2.h
strip target/release/libshim_v2.so
install -m 0755 target/release/libshim_v2.so ${RPM_BUILD_ROOT}/%{_libdir}/libshim_v2.so

%files
%defattr(-,root, root,-)
%{_libdir}/*

%files devel
%defattr(-,root, root,-)
%{_includedir}/shim_v2.h

%changelog
* Mon Mar 03 2025 laokz <zhangkai@iscas.ac.cn> - 0.0.1-11
- Type:Fix
- ID:NA
- SUG:NA
- DESC: riscv64: restore nix-0.16.1 patch

* Wed Jan 15 2025 kamizjw <zhongjiawei1@huawei.com> - 0.0.1-10
- Type:NA
- ID:NA
- SUG:NA
- DESC: sync some patches

* Thu Apr 18 2024 Pengda Dou <doupengda@loongson.cn> - 0.0.1-9
- Resolve loongarch64 patch addition error
- add support for loongarch64

* Fri Sep 01 2023 xuxuepeng <xuxuepeng1@huawei.com> - 0.0.1-8
- Type:Enhancement
- ID:NA
- SUG:NA
- DESC: Simplify Store api, remove unnecessary validation
        Check if a path is abstract before connection
        Add vsock support.

* Mon Jun 05 2023 laokz <zhangkai@iscas.ac.cn> - 0.0.1-7
- Type:Enhancement
- ID:NA
- SUG:NA
- DESC: Add riscv64 support. Patches are from nix v0.17.0 and v0.18.0.
        Add protobuf-compiler to avoid patching big binary for prost-build.

* Fri May 12 2023 Wenlong Zhang <zhangwenlong@loongson.cn> - 0.0.1-6
- Type: feature
- ID: NA
- SUG: NA
- DESC: Add loongarch64 architecture

* Fri Dec 09 2022 wujing <wujing50@huawei.com> - 0.0.1-5
- Type:improve
- ID:NA
- SUG:NA
- DESC: add yaml file

* Fri Sep 30 2022 wujing <wujing50@huawei.com> - 0.0.1-4
- Type:improve
- ID:NA
- SUG:NA
- DESC: strip libshim_v2.so

* Mon Jun 28 2021 gaohuatao <gaohuatao@huawei.com> - 0.0.1-3
- Type:NA
- ID:NA
- SUG:NA
- DESC:improve privileges

* Thu Jun 24 2021 gaohuatao <gaohuatao@huawei.com> - 0.0.1-2
- Type:NA
- ID:NA
- SUG:NA
- DESC:add Cargo.lock

* Mon Jun 21 2021 gaohuatao <gaohuatao@huawei.com> - 0.0.1
- Initial RPM release
